import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import TodoContext from "../context/TodoContext";
import PrinterPosIcon from "mdi-react/PrinterPosIcon";

const TransactionDetail = ({ match }) => {
  const { getDetailTransaction, detailById } = useContext(TodoContext);

  useEffect(() => {
    getDetailTransaction(match.params.id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, {});

  if (!detailById) {
    return null;
  }

  return (
    <div>
      <h1 className="font-300 text-center mb-5">Detail Transaksi</h1>
      <div className="card d-flex align-center mb-3">
        <p className="font-700">ID Transaksi: #{detailById.id}</p>
        <div
          className={
            detailById.status === "SUCCESS"
              ? "badge success ml-auto"
              : "badge check ml-auto"
          }
        >
          {detailById.status === "SUCCESS" ? "Berhasil" : "Pengecekan"}
        </div>
      </div>

      <div className="card d-flex mb-3">
        <div>
          <PrinterPosIcon
            size="80"
            style={{ color: "#fd6542", marginRight: "20px" }}
          />
        </div>
        <div>
          {/* PENGIRIM */}
          <div className="mb-3">
            <p className="text-uppercase mb-1">
              <b>PENGIRM</b>
            </p>
            <p className="text-uppercase">{detailById.sender_bank}</p>
          </div>

          {/* PENERIMA */}
          <div className="mb-3">
            <p className="text-uppercase mb-1">
              <b>PENERIMA</b>
            </p>
            <p className="mb-1 text-uppercase">{detailById.beneficiary_bank}</p>
            <p className="mb-1">{detailById.account_number}</p>
            <p>{detailById.beneficiary_name}</p>
          </div>

          {/* NOMINAL */}
          <div className="mb-3">
            <p className="text-uppercase mb-1">
              <b>NOMINAL</b>
            </p>
            <p className="mb-1">
              Rp{Intl.NumberFormat("de").format(detailById.amount)}
            </p>
            <p>
              <b>Kode Unik:</b> {detailById.unique_code}
            </p>
          </div>

          {/* CATATAN */}
          <div className="mb-3">
            <p className="text-uppercase mb-1">
              <b>CATATAN</b>
            </p>
            <p>{detailById.remark}</p>
          </div>

          {/* WAKTU DIBUAT */}
          <div className="mb-3">
            <p className="text-uppercase mb-1">
              <b>WAKTU DIBUAT</b>
            </p>
          </div>
        </div>
      </div>

      <Link to="/">
        <button type="button" className="btn-back">
          Kembali
        </button>
      </Link>
    </div>
  );
};

export default TransactionDetail;
