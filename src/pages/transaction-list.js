import React, { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import TodoContext from "../context/TodoContext";

import MagnifyIcon from "mdi-react/MagnifyIcon";
import ChevronDownIcon from "mdi-react/ChevronDownIcon";
import ArrowRightThickIcon from "mdi-react/ArrowRightThickIcon";
import ChevronUpIcon from "mdi-react/ChevronUpIcon";

const TransactionList = () => {
  const { getAllTransaction, dataTransactionAfter } = useContext(TodoContext);
  // const [dataTransaction, setDataTransaction] = useState([]);
  const [sortDropdown, setSortDropdown] = useState(false);
  const [sortSelected, setSortSelected] = useState("");
  const [sortType, setSortType] = useState("");
  const [search, setSearch] = useState("");

  useEffect(() => {
    getAllTransaction("", "default");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const sortFunc = (sortTypeFunc, sortTextFunc) => {
    setSortType(sortTypeFunc);
    setSortSelected(sortTextFunc);
    getAllTransaction(search, sortTypeFunc);
  };

  const handleChange = (event) => {
    setSearch(event.currentTarget.value);
    getAllTransaction(event.currentTarget.value, sortType);
  };

  return (
    <div>
      <h1 className="font-300 text-center mb-3">Daftar Transaksi</h1>
      <div className="px-3">
        <h2 className="font-700 mb-2">Halo Kak!</h2>
        <p className="mb-3" style={{ fontSize: "18px" }}>
          Kamu telah melakukan transaksi sebesar{" "}
          <span className="text-orange font-700">Rp.5.000.000</span> sejak
          menggunakan Flip.
        </p>
      </div>

      <div className="form-group">
        <input
          type="text"
          name="search"
          onChange={(e) => handleChange(e)}
          value={search}
          placeholder="Cari nama atau bank"
        ></input>
        <MagnifyIcon className="magnify" />
        <div className="sort" onClick={() => setSortDropdown(!sortDropdown)}>
          <span className="vh-center" style={{ left: "70%", width: "55%" }}>
            <span className="d-flex align-center">
              {sortSelected === "" ? "Urutkan" : sortSelected}{" "}
              {sortDropdown ? (
                <ChevronUpIcon
                  className="text-orange"
                  style={{ marginBottom: "-1px" }}
                />
              ) : (
                <ChevronDownIcon
                  className="text-orange"
                  style={{ marginBottom: "-1px" }}
                />
              )}
            </span>
          </span>
          <div
            className={
              sortDropdown ? "dropdown-sort show" : "dropdown-sort hide"
            }
          >
            <p onClick={() => sortFunc("asc", "Nama A-Z")}>Nama A-Z</p>
            <p onClick={() => sortFunc("desc", "Nama Z-A")}>Nama Z-A</p>
            <p onClick={() => sortFunc("newest", "Tanggal terbaru")}>
              Tanggal terbaru
            </p>
            <p onClick={() => sortFunc("oldest", "Tanggal terlama")}>
              Tanggal terlama
            </p>
          </div>
        </div>
      </div>

      {dataTransactionAfter.map((val) => {
        return (
          <Link to={`detail/${val.id}`}>
            <div
              className={
                val.status === "SUCCESS"
                  ? "card box-list-transaction success"
                  : "card box-list-transaction check"
              }
            >
              <div>
                <p className="mb-2 text-uppercase font-700">
                  {val.sender_bank} <ArrowRightThickIcon size="13" />{" "}
                  {val.beneficiary_bank}
                </p>
                <p className="text-uppercase mb-2">{val.beneficiary_name}</p>
                <p className="mb-0">
                  Rp{Intl.NumberFormat("de").format(val.amount)} •{" "}
                  {Intl.DateTimeFormat("id-ID", { dateStyle: "long" }).format(
                    new Date(val.created_at)
                  )}
                </p>
              </div>
              <div
                className={
                  val.status === "SUCCESS"
                    ? "badge success ml-auto"
                    : "badge check ml-auto"
                }
              >
                {val.status === "SUCCESS" ? "Berhasil" : "Pengecekan"}
              </div>
            </div>
          </Link>
        );
      })}
    </div>
  );
};

export default TransactionList;
