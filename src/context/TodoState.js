import React, { useReducer } from "react";
import TodoContext from "./TodoContext";
import TodoReducer from "./TodoReducer";
import {
  GET_ALL_TRANSACTION,
  GET_DETAIL_TRANSACTION_BYID,
  GET_DATA_TRANSACTION_AFTER,
} from "./TodoTypes";

const TodoState = ({ children }) => {
  const initialState = {
    allTransaction: [],
    dataTransactionAfter: [],
    detailById: {},
  };

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  const getAllTransaction = (search, sortType) => {
    let dataTransaction = dataTransactionAfter;
    let regex = new RegExp(search, "i");
    if (allTransaction.length === 0) {
      fetch("/frontend-test", {
        crossDomain: true,
        method: "GET",
        headers: { "Content-Type": "application/json" },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: GET_ALL_TRANSACTION,
            payload: Object.values(responseJson),
          });
          dispatch({
            type: GET_DATA_TRANSACTION_AFTER,
            payload: Object.values(responseJson),
          });
        });
    } else {
      if (sortType === "asc") {
        dataTransaction = allTransaction.sort((a, b) =>
          a.beneficiary_name.localeCompare(b.beneficiary_name)
        );
      } else if (sortType === "desc") {
        dataTransaction = allTransaction.sort((a, b) =>
          b.beneficiary_name.localeCompare(a.beneficiary_name)
        );
      } else if (sortType === "newest") {
        dataTransaction = allTransaction.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );
      } else if (sortType === "oldest") {
        dataTransaction = allTransaction.sort(
          (a, b) => new Date(a.created_at) - new Date(b.created_at)
        );
      } else {
        // eslint-disable-next-line no-self-assign
        dataTransaction = allTransaction;
      }
      dataTransaction = allTransaction.filter((item) =>
        regex.test(item.beneficiary_name)
      );
      dispatch({
        type: GET_DATA_TRANSACTION_AFTER,
        payload: dataTransaction,
      });
    }
  };

  // DETAIL DATA
  const getDetailTransaction = (id) => {
    const found = allTransaction.find((element) => element.id === id);
    dispatch({
      type: GET_DETAIL_TRANSACTION_BYID,
      payload: found,
    });
  };

  const { allTransaction, detailById, dataTransactionAfter } = state;

  return (
    <TodoContext.Provider
      value={{
        allTransaction,
        detailById,
        dataTransactionAfter,
        getAllTransaction,
        getDetailTransaction,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};

export default TodoState;
