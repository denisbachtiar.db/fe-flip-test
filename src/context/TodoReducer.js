import {
  GET_ALL_TRANSACTION,
  GET_DETAIL_TRANSACTION_BYID,
  GET_DATA_TRANSACTION_AFTER,
} from "./TodoTypes";

const Reducer = (state, { type, payload }) => {
  switch (type) {
    case GET_ALL_TRANSACTION:
      return {
        ...state,
        allTransaction: payload,
      };
    case GET_DETAIL_TRANSACTION_BYID:
      return {
        ...state,
        detailById: payload,
      };
    case GET_DATA_TRANSACTION_AFTER:
      return {
        ...state,
        dataTransactionAfter: payload,
      };
    default:
      return state;
  }
};

export default Reducer;
