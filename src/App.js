import { BrowserRouter, Route, Switch } from "react-router-dom";

// PAGES
import TransactionList from "./pages/transaction-list";
import TransactionDetail from "./pages/transaction-detail";

// CONTEXT
import TodoState from "./context/TodoState";

function App() {
  return (
    <TodoState>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <TransactionList />
            </Route>
            <Route
              path="/detail/:id"
              render={(props) => <TransactionDetail {...props} />}
            ></Route>
          </Switch>
        </BrowserRouter>
      </div>
    </TodoState>
  );
}

export default App;
